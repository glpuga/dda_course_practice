// =============================================================================
// Authors: Gerardo Puga
// Date: Aug 2019
// Description: Simple AND gate
// =============================================================================

module and_gate (output o_y, input i_in1, input i_in2);
  assign o_y = i_in1 & i_in2;
endmodule // and_gate
