# =============================================================================
# Authors: Gerardo Puga
# Date: Aug 2019
# Description: Simple AND gate
# =============================================================================

import cocotb
from cocotb.result import raise_error
from cocotb.triggers import Timer

@cocotb.test()
def and_input_output_sets(dut):
        """ Fist test, always succeed """

        dut._log.info("Running test started!")

        test_vector = [ ((0, 0), 0),
                        ((0, 1), 0),
                        ((1, 0), 0),
                        ((1, 1), 1)
        ]

        for inputs, output in test_vector:
                in1, in2 = inputs
                dut._log.info("Setting inputs to " + repr(inputs) + ", expecting " + repr(output))
                dut.i_in1 <= in1
                dut.i_in2 <= in2
                yield Timer(1)
                if (dut.o_y != output):
                    raise_error(dut, "for " + repr(inputs) + " expected " + repr(output) + " but got " + str(dut.o_y.value))

        dut._log.info("Running test finished!")
