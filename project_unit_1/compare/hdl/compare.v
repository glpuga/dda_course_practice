// =============================================================================
// Authors: Gerardo Puga
// Date: Aug 2019
// Description: Compare module to reset counter and enable shifter
// =============================================================================

module compare (output reg o_reset_counter,
                output reg o_enable_shift,
                input [31:0] i_reference,
                input [31:0] i_counter_value
               );

  always@(*) begin
    if (i_reference == i_counter_value)
    begin
      o_enable_shift <= 1;
      o_reset_counter <= 1;
    end else
    begin
      o_enable_shift <= 0;
      o_reset_counter <= 0;
    end
  end
endmodule
