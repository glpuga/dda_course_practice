# =============================================================================
# Authors: Gerardo Puga
# Date: Aug 2019
# Description: Compare module test
# =============================================================================

import cocotb
from cocotb.result import raise_error
from cocotb.triggers import Timer

@cocotb.test()
def test_compare_module_less_than(dut):
        " Test that the compare module works for the case counter < reference"
        dut.i_reference <= 100
        dut.i_counter_value <= 99
        yield Timer(10)
        outputs = dut.o_enable_shift.value, dut.o_reset_counter.value
        if (outputs != (0, 0)):
            raise_error(dut, "bad case: reference > counter")

@cocotb.test()
def test_compare_module_equal(dut):
        " Test that the compare module works for the case counter == reference"
        dut.i_reference <= 100
        dut.i_counter_value <= 100
        yield Timer(10)
        outputs = dut.o_enable_shift.value, dut.o_reset_counter.value
        if (outputs != (1, 1)):
            raise_error(dut, "bad case: reference == counter")
