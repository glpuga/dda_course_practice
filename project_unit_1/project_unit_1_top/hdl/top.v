// =============================================================================
// Authors: Gerardo Puga
// Date: Aug 2019
// Description: Top level entity for project 1
// =============================================================================

module shift_reg (output reg [3:0] o_led,
                  output reg [3:0] o_led_b,
                  output reg [3:0] o_led_g,
                  input [3:0] i_sw,
                  input i_reset,
                  input clock
                 );

    mod_counter u_mod_counter (
      .o_shift_enable(shift_enable_from_counter_to_shifter),
      .i_reference_selector(i_sw[2:1]),
      .i_count_enable(i_sw[0]),
      .i_async_reset(i_reset),
      .clock(clock)
    );

    shift_reg u_shift_reg (
      .o_led(led_outputs_from_shifter),
      .i_shift_enable(shift_enable_from_counter_to_shifter),
      .i_async_reset(i_reset),
      .clock(clock)
    );

    o_led = led_outputs_from_shifter;
    o_led_b = (~i_sw[3])?led_outputs_from_shifter:4'h0;
    o_led_g = i_sw[3]?led_outputs_from_shifter:4'h0;
endmodule
