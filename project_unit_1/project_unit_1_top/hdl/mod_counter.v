// =============================================================================
// Authors: Gerardo Puga
// Date: Aug 2019
// Description: Programmable module counter
// =============================================================================

module shift_reg (output reg o_shift_enable,
                  input i_reference_selector,
                  input i_count_enable,
                  input i_async_reset,
                  input clock
                 );
    wire counter_value_from_counter_to_compare_module;
    wire reset_from_compare_module_to_counter;
    wire reference_from_mux_to_compare_module;

    counter32 u_counter32(
      .o_current_value(counter_value_from_counter_to_compare_module),
      .i_reset_sync(reset_from_compare_module_to_counter)
      .i_enable(i_count_enable),
      .i_reset_async(i_async_reset),
      .clock(clock)
    );

    compare u_compare(
      .o_reset_counter(reset_from_compare_module_to_counter),
      .o_enable_shift(o_shift_enable),
      .i_reference(reference_from_mux_to_compare_module),
      .i_counter_value(counter_value_from_counter_to_compare_module)
    );

    ref_mux #(
      .REF_0(32'h00000001),
      .REF_1(32'h00000010),
      .REF_2(32'h00000100),
      .REF_3(32'h00001000) )
    u_ref_mux(
      .o_reference(reference_from_mux_to_compare_module),
      .i_selector(i_reference_selector)
    );
endmodule
