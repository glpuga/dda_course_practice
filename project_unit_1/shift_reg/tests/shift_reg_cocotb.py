# =============================================================================
# Authors: Gerardo Puga
# Date: Aug 2019
# Description: Compare module test
# =============================================================================

import cocotb
from cocotb.result import raise_error
from cocotb.triggers import Timer

@cocotb.coroutine
def step_clock(dut):
    yield Timer(5)
    dut.clock <= 1;
    yield Timer(5)
    dut.clock <= 0;
    yield Timer(5)

@cocotb.coroutine
def async_reset(dut):
    yield Timer(5)
    dut.i_async_reset <= 1;
    yield Timer(5)
    dut.i_async_reset <= 0;
    yield Timer(5)

@cocotb.test()
def test_shifter_initial_state(dut):
    " Test that the shifter's reset state turns the first led "
    dut.i_shift_enable <= 0
    dut.clock <= 0
    yield async_reset(dut)
    if (dut.o_led.value != 0x01):
        raise_error(dut, "bad reset state")

@cocotb.test()
def test_shifter_output_sequence(dut):
    " Test that the shifter rotates the bits when enable is set"
    dut.i_shift_enable <= 1
    dut.clock <= 0
    yield async_reset(dut)
    expected_outputs_vector = [ 0x01, 0x02, 0x04, 0x08, 0x01, 0x02 ]
    for expected_output in expected_outputs_vector:
        if (dut.o_led.value != expected_output):
            raise_error(dut, "expected output to be " + repr(expected_output)
                        + " but it's " + repr(dut.o_led.value) + " instead")
        yield step_clock(dut)

@cocotb.test()
def test_shifter_enable_works(dut):
    " Test that the shifter does not rotate when enable is not set"
    dut.i_shift_enable <= 0
    dut.clock <= 0
    yield async_reset(dut)
    expected_outputs_vector = [ 0x01, 0x01, 0x01, 0x01, 0x01, 0x01 ]
    for expected_output in expected_outputs_vector:
        if (dut.o_led.value != expected_output):
            raise_error(dut, "expected output to be " + repr(expected_output)
                        + " but it's " + repr(dut.o_led.value) + " instead")
        yield step_clock(dut)
