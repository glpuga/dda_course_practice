// =============================================================================
// Authors: Gerardo Puga
// Date: Aug 2019
// Description: 4 bit output left status shifter
// =============================================================================

module shift_reg (output reg [3:0] o_led,
                  input i_shift_enable,
                  input i_async_reset,
                  input clock
                 );

  always@(posedge clock, posedge i_async_reset) begin
    if (i_async_reset)
    begin
      o_led <= 4'b0001;
    end else
    begin
      if (i_shift_enable)
        o_led <= ((o_led << 1) & 4'b1110) | ((o_led >> 3) & 4'b0001);
      else
        o_led <= o_led;
    end
  end
endmodule
