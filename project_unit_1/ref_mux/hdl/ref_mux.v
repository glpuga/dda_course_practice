// =============================================================================
// Authors: Gerardo Puga
// Date: Aug 2019
// Description: reference selector with four configurable reference values
// =============================================================================

module ref_mux
  #(parameter REF_0 = 32'h00000000,
    parameter REF_1 = 32'h00000011,
    parameter REF_2 = 32'h00000022,
    parameter REF_3 = 32'h00000033 )
    (output reg [31:0] o_reference,
     input [1:0] i_selector
    );
  always@(*) begin
    case(i_selector)
      2'b00 : o_reference <= REF_0;
      2'b01 : o_reference <= REF_1;
      2'b10 : o_reference <= REF_2;
      default : o_reference <= REF_3;
    endcase
  end
endmodule
