# =============================================================================
# Authors: Gerardo Puga
# Date: Aug 2019
# Description: Simple AND gate
# =============================================================================

import cocotb
from cocotb.result import raise_error
from cocotb.triggers import Timer

@cocotb.test()
def test_mux_default_references(dut):
        " Test that the reference mux works "
        test_vector = [
            (0x00, 0x00),
            (0x01, 0x11),
            (0x02, 0x22),
            (0x03, 0x33)
        ]
        for input, expected_output in test_vector:
            dut.i_selector <= input
            yield Timer(10)
            if (dut.o_reference.value != expected_output):
                raise_error(dut, "expected " + repr(expected_output) + " but got " + str(dut.o_reference.value))
