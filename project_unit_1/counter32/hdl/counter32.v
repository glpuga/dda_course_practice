// =============================================================================
// Authors: Gerardo Puga
// Date: Aug 2019
// Description: 32 bits counter with enable and sync and async resets
// =============================================================================

module counter32 (output reg [31:0] o_current_value,
                  input i_reset_sync,
                  input i_enable,
                  input i_reset_async,
                  input clock
                 );

  always@(posedge clock, posedge i_reset_async) begin
    if (i_reset_async)
      o_current_value <= 32'b0;
    else if (i_reset_sync)
      o_current_value <= 32'b0;
    else if (i_enable)
      o_current_value <= o_current_value + 1;
    else
      o_current_value <= o_current_value;
  end
endmodule // and_gate
