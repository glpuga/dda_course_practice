# =============================================================================
# Authors: Gerardo Puga
# Date: Aug 2019
# Description: Simple AND gate
# =============================================================================

import cocotb
from cocotb.result import raise_error
from cocotb.triggers import Timer

@cocotb.coroutine
def clock_step(dut):
    yield Timer(5)
    dut.clock <= 1;
    yield Timer(10)
    dut.clock <= 0;
    yield Timer(5)


@cocotb.coroutine
def async_reset(dut):
    yield Timer(5)
    dut.i_reset_async <= 1;
    yield Timer(5)
    dut.i_reset_async <= 0;
    yield Timer(5)

@cocotb.test()
def test_counter_increases(dut):
        " Test that the counter can increment value "
        dut.clock <= 0;
        dut.i_enable <= 1;
        dut.i_reset_sync <= 0;
        yield async_reset(dut)
        for current_value in range(1, 128):
            yield clock_step(dut)
            if (dut.o_current_value.value != current_value):
                raise_error(dut, "expected " + repr(current_value) + " but got " + str(dut.o_current_value.value))

@cocotb.test()
def test_async_reset_works(dut):
        " Test that the async reset works "
        dut.clock <= 0;
        dut.i_enable <= 1;
        dut.i_reset_sync <= 0;
        yield async_reset(dut)
        for _ in range(1, 128):
            yield clock_step(dut)
        if (dut.o_current_value != 127):
            raise_error(dut, "expected the count to have reached 127, but it's " + str(dut.o_current_value.value) + " instead")
        yield async_reset(dut)
        if (dut.o_current_value != 0):
            raise_error(dut, "expected the count to have been reset to 0, but it's " + str(dut.o_current_value.value) + " instead")

@cocotb.test()
def test_sync_reset_works(dut):
        " Test that the async reset works "
        dut.clock <= 0;
        dut.i_enable <= 1;
        dut.i_reset_sync <= 0;
        yield async_reset(dut)
        for _ in range(1, 128):
            yield clock_step(dut)
        if (dut.o_current_value != 127):
            raise_error(dut, "expected the count to have reached 127, but it's " + str(dut.o_current_value.value) + " instead")
        dut.i_reset_sync <= 1
        if (dut.o_current_value != 127):
            raise_error(dut, "async reset executed before its time")
        yield clock_step(dut)
        if (dut.o_current_value != 0):
            raise_error(dut, "async reset failed to reinitialized the counter")

@cocotb.test()
def test_enable_works(dut):
        " Test that the enable input works "
        dut.clock <= 0;
        dut.i_enable <= 0;
        dut.i_reset_sync <= 0;
        yield async_reset(dut)
        for _ in range(1, 128):
            yield clock_step(dut)
        if (dut.o_current_value != 0):
            raise_error(dut, "expected the count to still be 0, but it's " + str(dut.o_current_value.value) + " instead")
