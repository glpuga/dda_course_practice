// =============================================================================
// Authors: Gerardo Puga
// Date: Aug 2019
// Description: Simple XOR gate instantiating other components
// =============================================================================

module xor_gate (output o_y, input i_in1, input i_in2);
  wire from_and1_to_and;
  wire from_and2_to_and;

  or_gate u_or_gate(.o_y(o_y), .i_in1(from_and1_to_and), .i_in2(from_and2_to_and));
  and_gate u_and1_gate(.o_y(from_and1_to_and), .i_in1(~i_in1), .i_in2(i_in2));
  and_gate u_and2_gate(.o_y(from_and2_to_and), .i_in1(i_in1), .i_in2(~i_in2));
endmodule // and_gate
